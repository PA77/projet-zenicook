import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Recipe} from '../models/recipe';
import {RecipeCreate} from '../models/RecipeCreate';

@Injectable()
export class RecipeService {

  constructor(
    private http: HttpClient,
  ) {
  }

  getRecipes(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>('http://localhost:8080/recipes');
  }

  getOneRecipe(id: string): Observable<Recipe> {
    return this.http.get<Recipe>(`http://localhost:8080/recipes/${id}`);
  }

  createRecipe(recipe: RecipeCreate): Observable<RecipeCreate> {
    return this.http.post<RecipeCreate>(`http://localhost:8080/recipes`, recipe);
  }

  randomRecipe(): Observable<Recipe> {
    return this.http.get<Recipe>('http://localhost:8080/recipes/random');
  }



}
