import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Ingredient} from '../models/ingredient';
import {IngredientCreate} from '../models/IngredientCreate';

@Injectable()
export class IngredientService {

  constructor(
    private http: HttpClient
  ) {
  }

  getIngredients(): Observable<Ingredient[]> {
    return this.http.get<Ingredient[]>('http://localhost:8080/ingredients');
  }

  createIngredient(ingredient: IngredientCreate): Observable<IngredientCreate> {
    return this.http.post<IngredientCreate>(`http://localhost:8080/ingredients`, ingredient);

  }
}
