import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RecipeComponent } from './components/recipe/recipe.component';
import { ListRecipesComponent } from './components/list-recipes/list-recipes.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RecipeService} from './service/recipe.service';
import {IngredientService} from './service/ingredient.service';
import { ListIngredientsComponent } from './components/list-ingredients/list-ingredients.component';
import { IngredientComponent } from './components/ingredient/ingredient.component';
import { RecipeDetailsComponent } from './components/recipe-details/recipe-details.component';
import { AdminGestionComponent } from './components/admin-gestion/admin-gestion.component';
import { RecipeCreateComponent } from './components/recipe-create/recipe-create.component';
import { RandomRecipeComponent } from './components/random-recipe/random-recipe.component';

const appRoutes: Routes = [
  { path: 'recipes', component: ListRecipesComponent },
  { path: 'ingredients', component: ListIngredientsComponent },
  { path: 'details/:id', component: RecipeDetailsComponent},
  { path: 'admin', component: AdminGestionComponent},
  { path: 'creation', component: RecipeCreateComponent},
  { path: 'random', component: RandomRecipeComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    RecipeComponent,
    ListRecipesComponent,
    ListIngredientsComponent,
    IngredientComponent,
    RecipeDetailsComponent,
    AdminGestionComponent,
    RecipeCreateComponent,
    RandomRecipeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    RecipeService,
    IngredientService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
