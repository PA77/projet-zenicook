import {Ingredient} from './ingredient';
import {Step} from './step';

export class Recipe {
  id: string;
  name: string;
  recipeContent: string;
  cookingTime: number;
  preparationTime: number;
  difficulty: number;
  portions: number;
  pictureUrl: string;
  steps: Step[];
  ingredients: Ingredient[];

  constructor(name: string, recipeContent: string, cookingTime: number, preparationTime: number, difficulty: number,
              portions: number, pictureUrl: string, steps: Step[], ingredients: Ingredient[]) {
    this.name = name;
    this.recipeContent = recipeContent;
    this.cookingTime = cookingTime;
    this.preparationTime = preparationTime;
    this.difficulty = difficulty;
    this.portions = portions;
    this.pictureUrl = pictureUrl;
    this.ingredients = ingredients;
  }
}
