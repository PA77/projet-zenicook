import {Step} from './step';
import {Ingredient} from './ingredient';

export class RecipeCreate {
  name: string;
  recipeContent: string;
  cookingTime: number;
  preparationTime: number;
  difficulty: number;
  portions: number;
  pictureUrl: string;
  steps: Step[];
  ingredients: Ingredient[];

  constructor(name: string, recipeContent: string, cookingTime: number, preparationTime, difficulty: number, portions: number,
              pictureUrl: string, steps: Step[], ingredients: Ingredient[]) {
    this.name = name;
    this.recipeContent = recipeContent;
    this.cookingTime = cookingTime;
    this.preparationTime = preparationTime;
    this.difficulty = difficulty;
    this.portions = portions;
    this.pictureUrl = pictureUrl;
    this.steps = steps;
    this.ingredients = ingredients;
  }
}
