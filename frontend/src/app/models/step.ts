export class Step {
  orders: number;
  description: string;

  constructor(orders: number, description: string) {
    this.orders = orders;
    this.description = description;
  }

}
