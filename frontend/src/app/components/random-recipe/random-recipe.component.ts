import { Component, OnInit } from '@angular/core';
import {RecipeService} from '../../service/recipe.service';
import {Recipe} from '../../models/recipe';

@Component({
  selector: 'app-random-recipe',
  templateUrl: './random-recipe.component.html',
  styleUrls: ['./random-recipe.component.css']
})
export class RandomRecipeComponent implements OnInit {

  recipe: Recipe;

  constructor(private recipeService: RecipeService) { }

  ngOnInit() {
    this.recipeService.randomRecipe().subscribe(result => {
      this.recipe = result;
      });
  }

}
