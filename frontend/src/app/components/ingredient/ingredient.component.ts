import {Component, Input, OnInit} from '@angular/core';
import {Ingredient} from '../../models/ingredient';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent {
  @Input() ingredient: Ingredient;

  constructor() { }

}
