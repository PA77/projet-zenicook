import { Component, OnInit } from '@angular/core';
import {RecipeService} from '../../service/recipe.service';
import {Recipe} from '../../models/recipe';

@Component({
  selector: 'app-list-recipes',
  templateUrl: './list-recipes.component.html',
  styleUrls: ['./list-recipes.component.css']
})
export class ListRecipesComponent implements OnInit {

  recipes: Recipe[];

  constructor(private recipeService: RecipeService) { }

  ngOnInit() {
    this.recipeService.getRecipes().subscribe(recipesList =>
    this.recipes = recipesList);
  }

}
