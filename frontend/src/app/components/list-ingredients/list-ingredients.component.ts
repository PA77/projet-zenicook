import {Component, OnInit} from '@angular/core';
import {Ingredient} from '../../models/ingredient';
import {IngredientService} from '../../service/ingredient.service';
import {IngredientCreate} from '../../models/IngredientCreate';

@Component({
  selector: 'app-list-ingredients',
  templateUrl: './list-ingredients.component.html',
  styleUrls: ['./list-ingredients.component.css']
})
export class ListIngredientsComponent implements OnInit {

  ingredients: Ingredient[];
  ingredientName: string;
  ingredientCreate: IngredientCreate;
  creationOk = false;

  constructor(private ingredientService: IngredientService) {
  }

  ngOnInit() {
    this.ingredientService.getIngredients().subscribe(ingredientsList =>
      this.ingredients = ingredientsList);
  }

  save() {
    this.ingredientCreate = new IngredientCreate(this.ingredientName);
    this.ingredientService.createIngredient(this.ingredientCreate).subscribe(() => {
      this.creationOk = true;
    });
  }

}
