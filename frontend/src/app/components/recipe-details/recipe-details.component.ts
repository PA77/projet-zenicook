import { Component, OnInit } from '@angular/core';
import {Recipe} from '../../models/recipe';
import {RecipeService} from '../../service/recipe.service';
import {ActivatedRoute} from '@angular/router';
import {Ingredient} from '../../models/ingredient';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {

  recipe: Recipe;
  id: string;
  ingredients: Ingredient[] = [];

  constructor(private recipeService: RecipeService, private route: ActivatedRoute) { }

  ngOnInit() {
      this.id = this.route.snapshot.paramMap.get('id');
      this.recipeService.getOneRecipe(this.id).subscribe(result => {
      this.recipe = result;
      });

  }



}
