import {Component, OnInit} from '@angular/core';
import {RecipeCreate} from '../../models/RecipeCreate';
import {Step} from '../../models/step';
import {Ingredient} from '../../models/ingredient';
import {RecipeService} from '../../service/recipe.service';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {IngredientCreate} from '../../models/IngredientCreate';

@Component({
  selector: 'app-recipe-create',
  templateUrl: './recipe-create.component.html',
  styleUrls: ['./recipe-create.component.css']
})
export class RecipeCreateComponent implements OnInit {
  public invoiceForm: FormGroup;
  public invoiceForm2: FormGroup;

  recipeCreate: RecipeCreate;
  name: string;
  recipeContent: string;
  cookingTime: number;
  preparationTime: number;
  difficulty: number;
  portions: number;
  pictureUrl: string;
  steps: Step[] = [];
  ingredients: Ingredient[] = [];
  step1: Step;
  order1: number;
  description: string;
  creationOk = false;

  constructor(private recipeService: RecipeService, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.invoiceForm = this.fb.group({
      ingredients: this.fb.array([this.initItemRows()])
    });
    this.invoiceForm2 = this.fb.group({
      steps: this.fb.array([this.initItemRows2()])
    });
  }

  get formArr() {
    return this.invoiceForm.get('ingredients') as FormArray;
  }

  get formArrsteps() {
    return this.invoiceForm2.get('steps') as FormArray;
  }

  initItemRows() {
    return this.fb.group({
      name: [''],
      quantity: [],
      unit: ['']
    });
  }

  initItemRows2() {
    return this.fb.group({
      order: [],
      description: ['']
    });
  }


  addNewRow() {
    this.formArr.push(this.initItemRows());
  }

  addNewRow2() {
    this.formArrsteps.push(this.initItemRows2());
  }

  deleteRow(index: number) {
    this.formArr.removeAt(index);
  }

  deleteRow2(index: number) {
    this.formArrsteps.removeAt(index);
  }

save() {
  this.invoiceForm.controls.ingredients.value.forEach(ingredient => {
    this.ingredients.push(new Ingredient(ingredient.name, ingredient.quantity, ingredient.unit));
  });
  this.invoiceForm2.controls.steps.value.forEach(step => {
    this.steps.push(new Step(step.order, step.description));
  });
  this.recipeCreate = new RecipeCreate(this.name, this.recipeContent, this.cookingTime, this.preparationTime, this.difficulty,
    this.portions, this.pictureUrl, this.steps, this.ingredients);
  this.creationOk = true;
  return this.recipeService.createRecipe(this.recipeCreate).subscribe();
  }
}
