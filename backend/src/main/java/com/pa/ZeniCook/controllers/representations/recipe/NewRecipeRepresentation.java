package com.pa.ZeniCook.controllers.representations.recipe;

import com.pa.ZeniCook.controllers.representations.recipeingredient.NewRecipeIngredientRepresentation;
import com.pa.ZeniCook.domains.RecipeIngredient;
import com.pa.ZeniCook.domains.Step;

import java.util.List;

public class NewRecipeRepresentation {

    private String id;
    private String name;
    private String recipeContent;
    private int cookingTime;
    private int preparationTime;
    private int difficulty;
    private int portions;
    private String pictureUrl;
    private List<Step> steps;
    private List<NewRecipeIngredientRepresentation> ingredients;

    public NewRecipeRepresentation(String id, String name, String recipeContent, int cookingTime, int preparationTime, int difficulty,
                                   int proportion, String pictureUrl, List<Step> steps, List<NewRecipeIngredientRepresentation> ingredients){
        this.id = id;
        this.name = name;
        this. recipeContent = recipeContent;
        this.cookingTime = cookingTime;
        this.preparationTime = preparationTime;
        this.difficulty = difficulty;
        this.portions = proportion;
        this.pictureUrl = pictureUrl;
        this.steps = steps;
        this.ingredients = ingredients;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipeContent() {
        return recipeContent;
    }

    public void setRecipeContent(String recipeContent) {
        this.recipeContent = recipeContent;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(int cookingTime) {
        this.cookingTime = cookingTime;
    }

    public int getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(int preparationTime) {
        this.preparationTime = preparationTime;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getPortions() {
        return portions;
    }

    public void setPortions(int portions) {
        this.portions = portions;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public List<NewRecipeIngredientRepresentation> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<NewRecipeIngredientRepresentation> ingredients) {
        this.ingredients = ingredients;
    }
}
