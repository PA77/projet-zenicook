package com.pa.ZeniCook.controllers.representations.recipe;

import com.pa.ZeniCook.controllers.representations.ingredient.CreationIngredientRepresentation;
import com.pa.ZeniCook.controllers.representations.step.CreationStepRepresentation;
import com.pa.ZeniCook.domains.RecipeIngredient;

import java.util.List;

public class CreationRecipeRepresentation {
    private String name;
    private String recipeContent;
    private int cookingTime;
    private int preparationTime;
    private int difficulty;
    private int portions;
    private String pictureUrl;
    private List<CreationStepRepresentation> steps;
    private List<CreationIngredientRepresentation> ingredients;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipeContent() {
        return recipeContent;
    }

    public void setRecipeContent(String recipeContent) {
        this.recipeContent = recipeContent;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(int cookingTime) {
        this.cookingTime = cookingTime;
    }

    public int getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(int preparationTime) {
        this.preparationTime = preparationTime;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getPortions() {
        return portions;
    }

    public void setPortions(int portions) {
        this.portions = portions;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public List<CreationStepRepresentation> getSteps() {
        return steps;
    }

    public void setSteps(List<CreationStepRepresentation> steps) {
        this.steps = steps;
    }

    public List<CreationIngredientRepresentation> getIngredients() {
        return ingredients;
    }

    public void setIngredientList(List<CreationIngredientRepresentation> ingredients) {
        this.ingredients = ingredients;
    }
}
