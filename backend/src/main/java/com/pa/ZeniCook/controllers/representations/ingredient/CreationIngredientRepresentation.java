package com.pa.ZeniCook.controllers.representations.ingredient;

public class CreationIngredientRepresentation {

    private String id;
    private String name;
    private int quantity;
    private String unit;

    public CreationIngredientRepresentation(String id, String name, int quantity, String unit){
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
