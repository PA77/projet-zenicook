package com.pa.ZeniCook.domains;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "recipes")
public class Recipe implements Serializable {

    @Id
    private String id;
    private String name;
    private String recipeContent;
    private int cookingTime;
    private int preparationTime;
    private int difficulty;
    private int portions;
    private String pictureUrl;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "recipe_id")
    private List<Step> stepList;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,
    mappedBy = "recipe", orphanRemoval = true)
    private List<RecipeIngredient> ingredients;

    protected Recipe(){}

    public Recipe(String id, String name, String recipeContent, int cookingTime, int preparationTime, int difficulty,
                  int proportion, String pictureUrl, List<Step> stepList, List<RecipeIngredient> ingredients){
        this.id = id;
        this.name = name;
        this. recipeContent = recipeContent;
        this.cookingTime = cookingTime;
        this.preparationTime = preparationTime;
        this.difficulty = difficulty;
        this.portions = proportion;
        this.pictureUrl = pictureUrl;
        this.stepList = stepList;
        this.ingredients = ingredients;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipeContent() {
        return recipeContent;
    }

    public void setRecipeContent(String recipeContent) {
        this.recipeContent = recipeContent;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(int cookingTime) {
        this.cookingTime = cookingTime;
    }

    public int getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(int preparationTime) {
        this.preparationTime = preparationTime;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getPortions() {
        return portions;
    }

    public void setPortions(int portions) {
        this.portions = portions;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public List<Step> getStepList() {
        return stepList;
    }

    public void setStepList(List<Step> stepList) {
        this.stepList = stepList;
    }

    public List<RecipeIngredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<RecipeIngredient> ingredients) {
        this.ingredients = ingredients;
    }
}
